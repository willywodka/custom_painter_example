import 'package:flutter/material.dart';

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = Colors.red;
    paint.strokeWidth = 2.0;
    Offset center = const Offset(100.0, 100.0);
    //Offset secondCenter = const Offset(-50.0, 100.0);
    canvas.drawCircle(center, 50.0, paint);
    //canvas.drawCircle(secondCenter, 40.0, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}



class MyForegroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = Colors.blue;
    paint.strokeWidth = 2.0;
    Offset center = const Offset(00.0, 10.0);
    //Offset secondCenter = const Offset(-50.0, 100.0);
    canvas.drawCircle(center, 50.0, paint);
    //canvas.drawCircle(secondCenter, 40.0, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
