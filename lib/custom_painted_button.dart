import 'package:flutter/material.dart';

class CustomPaintedButton extends StatelessWidget {

  const CustomPaintedButton({super.key, this.onTap});

  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
    onTap: onTap,
    child: SizedBox(
      height: 50,
      width: 100,
      child: ClipRect(
        child: CustomPaint(
          painter: ButtonPainter(text: 'Кнопка'),
        ),
      ),
    ),
  );
}

class ButtonPainter extends CustomPainter {
  ButtonPainter({
    this.text,
    this.textStyle,
    this.textAlign,
    this.textDirection
  });

  final String? text;
  final TextStyle? textStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = Colors.red;
    paint.strokeWidth = 2.0;
    canvas.drawOval(
        Rect.fromCenter(
          center: size.center(Offset.zero),
          width: size.width,
          height: size.height,
        ),
        paint);
    TextSpan textSpan = TextSpan(
      text: text,
      style: textStyle,
    );
    TextPainter textPainter = TextPainter(
      text: textSpan,
      textAlign: textAlign ?? TextAlign.center,
      textDirection: textDirection ?? TextDirection.ltr,
    );
    textPainter.layout();
    final xCenter = (size.width - textPainter.width) / 2;
    final yCenter = (size.height - textPainter.height) / 2;
    final textOffset = Offset(xCenter, yCenter);
    textPainter.paint(canvas, textOffset);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}